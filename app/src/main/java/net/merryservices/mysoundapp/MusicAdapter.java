package net.merryservices.mysoundapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MusicAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Music> musics;

    public MusicAdapter(Context context) {
        this.context = context;
        musics = new ArrayList<Music>();
    }

    public void setMusics(ArrayList<Music> musics) {
        this.musics = musics;
    }

    @Override
    public int getCount() {
        return musics.size();
    }

    @Override
    public Object getItem(int position) {
        return musics.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService((Context.LAYOUT_INFLATER_SERVICE));
        View rowView = inflater.inflate(R.layout.item_music, null);
        TextView textArtiste= (TextView) rowView.findViewById(R.id.textViewArtiste);
        textArtiste.setText(musics.get(position).getArtiste());
        TextView textTitle = (TextView) rowView.findViewById((R.id.textViewTitle));
        textTitle.setText(musics.get(position).getTitle());
        ImageView imageView= (ImageView) rowView.findViewById(R.id.imageViewSound);
        ServerAPI.loadImage(context, musics.get(position).getImage(), imageView);
        return rowView;
    }
}
