package net.merryservices.mysoundapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ListView listView;
    EditText editSearch;
    Button buttonSearch;
    MusicAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView= (ListView) findViewById(R.id.listMusics);
        editSearch = (EditText) findViewById(R.id.editTextSearch);
        buttonSearch= (Button) findViewById(R.id.buttonSearch);
        buttonSearch.setOnClickListener(this);
        adapter= new MusicAdapter(this);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Music music = (Music) parent.getItemAtPosition(position);
                Intent intent= new Intent(getApplicationContext(), MusicActivity.class);
                intent.putExtra("music", music);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {
        String search= editSearch.getText().toString();
        ServerAPI.getMusics(search, this, new IListenerAPI() {
            @Override
            public void receiveMusics(ArrayList<Music> musics) {
                adapter.setMusics(musics);
                adapter.notifyDataSetChanged();
            }
        });
    }
}
