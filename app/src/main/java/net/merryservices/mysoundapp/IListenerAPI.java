package net.merryservices.mysoundapp;

import java.util.ArrayList;

public interface IListenerAPI {
    public void receiveMusics(ArrayList<Music> musics);
}
