package net.merryservices.mysoundapp;

import java.io.Serializable;

public class Music implements Serializable {

    private String album;
    private String artiste;
    private String title;
    private String image;
    private String preview;
    private String link;

    public Music(){

    }

    public Music(String artiste, String title) {
        this.artiste = artiste;
        this.title = title;
    }

    public Music(String album, String artiste, String title, String image, String preview) {
        this.album = album;
        this.artiste = artiste;
        this.title = title;
        this.image = image;
        this.preview = preview;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtiste() {
        return artiste;
    }

    public void setArtiste(String artiste) {
        this.artiste = artiste;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }
}
