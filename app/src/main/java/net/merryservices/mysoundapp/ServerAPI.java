package net.merryservices.mysoundapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ServerAPI {

    private  static String URL_API="http://api.deezer.com/search?q=";


    public static void getMusics(String search, Context context, final IListenerAPI listener){
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_API + search,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ArrayList<Music> musics= new ArrayList<Music>();
                            JSONObject jsonObject= new JSONObject(response);
                            JSONArray tracks= jsonObject.getJSONArray("data");
                            for(int i=0; i< tracks.length(); i++){
                                JSONObject track= tracks.getJSONObject(i);
                                Music m = new Music();
                                m.setAlbum(track.getJSONObject("album").getString("title"));
                                m.setArtiste(track.getJSONObject("artist").getString("name"));
                                m.setTitle(track.getString("title"));
                                m.setImage(track.getJSONObject("album").getString("cover_small"));
                                m.setPreview(track.getString("preview"));
                                m.setLink(track.getString("link"));
                                musics.add(m);
                            }
                            listener.receiveMusics(musics);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.d("SERVERAPI", "error lors de la requete");
                    }
                });
        queue.add(stringRequest);
    }

    public static void loadImage(Context context, String url, final ImageView imageView){
        RequestQueue queue = Volley.newRequestQueue(context);
        ImageRequest request = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        imageView.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        imageView.setImageResource(R.drawable.ic_launcher_foreground);
                    }
                });
        queue.add(request);
    }


}
