package net.merryservices.mysoundapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

public class MusicActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imageView;
    TextView textArtiste, textAlbum, textTitre;
    Music currentMusic;
    Button buttonLink, buttonPlay;
    MediaPlayer player= new MediaPlayer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);

        imageView = (ImageView) findViewById(R.id.imageViewDetailSound);
        textArtiste= (TextView) findViewById(R.id.textViewDetailArtiste);
        textAlbum= (TextView) findViewById(R.id.textViewDetailAlbum);
        textTitre= (TextView) findViewById(R.id.textViewDetailTitre);
        buttonLink= (Button) findViewById(R.id.buttonLink);
        buttonLink.setOnClickListener(this);
        buttonPlay= (Button) findViewById(R.id.buttonPlay);
        buttonPlay.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        Music m= (Music) intent.getSerializableExtra("music");
        refresh(m);
        currentMusic=m;
    }

    private void refresh(Music m){
        textArtiste.setText(m.getArtiste());
        textTitre.setText(m.getTitle());
        textAlbum.setText(m.getAlbum());
        ServerAPI.loadImage(this, m.getImage(), imageView);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(buttonLink)){
            Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(currentMusic.getLink()));
            startActivity(intent);
        }else{
            if(!player.isPlaying()){
                try {
                    player.reset();
                    player.setDataSource(this, Uri.parse(currentMusic.getPreview()));
                    player.prepare();
                    player.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                player.stop();
            }
        }
    }
}
